﻿namespace Kapa_pro_triplex
{
    partial class hlavniOknoTriplexForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(hlavniOknoTriplexForm));
            this.nyniLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.seznamReceptuListBox = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.datumPoslAktLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.smesLabel = new System.Windows.Forms.Label();
            this.proRozmeryLabel = new System.Windows.Forms.Label();
            this.barevneZnaceniLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.polotovarLabel = new System.Windows.Forms.Label();
            this.cisloReceptuLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox70 = new System.Windows.Forms.GroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.smes4NumericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.smes4NumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox120 = new System.Windows.Forms.GroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.smes3NumericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.smes3NumericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.smes3NumericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.smes3NumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox150 = new System.Windows.Forms.GroupBox();
            this.smes2NumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.smes2NumericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.smes2NumericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.smes2NumericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.groupBox220 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.smes1NumericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.smes1NumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.smes1NumericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.smes1NumericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label20 = new System.Windows.Forms.Label();
            this.smes1NumericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.smes1NumericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.smes1NumericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.smes1NumericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.smenyComboBox = new System.Windows.Forms.ComboBox();
            this.obsluhaComboBox = new System.Windows.Forms.ComboBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cisloPruvodkyLabel = new System.Windows.Forms.Label();
            this.zpracDoLabel = new System.Windows.Forms.Label();
            this.zpracOdLabel = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.vyrobilTextBox = new System.Windows.Forms.TextBox();
            this.smesLabel2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.proRozmeryLabel2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.tiskButton1 = new System.Windows.Forms.Button();
            this.pruvodkaPolotovaruLabel = new System.Windows.Forms.Label();
            this.cisloReceptuLabel2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.seznamPrůvodekToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.konecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zobrazitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nastaveníToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nápovědaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.barZnacTiskLabel = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox70.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes4NumericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes4NumericUpDown1)).BeginInit();
            this.groupBox120.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown1)).BeginInit();
            this.groupBox150.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown3)).BeginInit();
            this.groupBox220.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown8)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // nyniLabel
            // 
            this.nyniLabel.AutoSize = true;
            this.nyniLabel.Location = new System.Drawing.Point(75, 30);
            this.nyniLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nyniLabel.Name = "nyniLabel";
            this.nyniLabel.Size = new System.Drawing.Size(75, 13);
            this.nyniLabel.TabIndex = 9;
            this.nyniLabel.Text = "nyníLabelText";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.seznamReceptuListBox);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(12, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 441);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Výběr receptu";
            // 
            // seznamReceptuListBox
            // 
            this.seznamReceptuListBox.FormattingEnabled = true;
            this.seznamReceptuListBox.ItemHeight = 20;
            this.seznamReceptuListBox.Location = new System.Drawing.Point(2, 26);
            this.seznamReceptuListBox.Name = "seznamReceptuListBox";
            this.seznamReceptuListBox.Size = new System.Drawing.Size(167, 404);
            this.seznamReceptuListBox.TabIndex = 1;
            this.seznamReceptuListBox.SelectedIndexChanged += new System.EventHandler(this.seznamReceptuListBox_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.datumPoslAktLabel);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.smesLabel);
            this.groupBox2.Controls.Add(this.proRozmeryLabel);
            this.groupBox2.Controls.Add(this.barevneZnaceniLabel);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.polotovarLabel);
            this.groupBox2.Controls.Add(this.cisloReceptuLabel);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(193, 50);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(233, 441);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Recept:";
            // 
            // datumPoslAktLabel
            // 
            this.datumPoslAktLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.datumPoslAktLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.datumPoslAktLabel.Location = new System.Drawing.Point(57, 120);
            this.datumPoslAktLabel.Name = "datumPoslAktLabel";
            this.datumPoslAktLabel.Size = new System.Drawing.Size(175, 20);
            this.datumPoslAktLabel.TabIndex = 11;
            this.datumPoslAktLabel.Text = "label";
            this.datumPoslAktLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(5, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(176, 16);
            this.label5.TabIndex = 10;
            this.label5.Text = "Datum poslední aktualizace:";
            // 
            // smesLabel
            // 
            this.smesLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.smesLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smesLabel.Location = new System.Drawing.Point(57, 60);
            this.smesLabel.Name = "smesLabel";
            this.smesLabel.Size = new System.Drawing.Size(175, 20);
            this.smesLabel.TabIndex = 9;
            this.smesLabel.Text = "label11";
            this.smesLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // proRozmeryLabel
            // 
            this.proRozmeryLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.proRozmeryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.proRozmeryLabel.Location = new System.Drawing.Point(5, 160);
            this.proRozmeryLabel.Name = "proRozmeryLabel";
            this.proRozmeryLabel.Size = new System.Drawing.Size(227, 270);
            this.proRozmeryLabel.TabIndex = 8;
            this.proRozmeryLabel.Text = "label10";
            this.proRozmeryLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // barevneZnaceniLabel
            // 
            this.barevneZnaceniLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.barevneZnaceniLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.barevneZnaceniLabel.Location = new System.Drawing.Point(122, 80);
            this.barevneZnaceniLabel.Name = "barevneZnaceniLabel";
            this.barevneZnaceniLabel.Size = new System.Drawing.Size(110, 20);
            this.barevneZnaceniLabel.TabIndex = 7;
            this.barevneZnaceniLabel.Text = "label9";
            this.barevneZnaceniLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(5, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 16);
            this.label8.TabIndex = 6;
            this.label8.Text = "Barevné značení:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(5, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 16);
            this.label7.TabIndex = 5;
            this.label7.Text = "Směs:";
            // 
            // polotovarLabel
            // 
            this.polotovarLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.polotovarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.polotovarLabel.Location = new System.Drawing.Point(80, 40);
            this.polotovarLabel.Name = "polotovarLabel";
            this.polotovarLabel.Size = new System.Drawing.Size(152, 20);
            this.polotovarLabel.TabIndex = 4;
            this.polotovarLabel.Text = "label6";
            this.polotovarLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cisloReceptuLabel
            // 
            this.cisloReceptuLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cisloReceptuLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloReceptuLabel.Location = new System.Drawing.Point(102, 20);
            this.cisloReceptuLabel.Name = "cisloReceptuLabel";
            this.cisloReceptuLabel.Size = new System.Drawing.Size(130, 20);
            this.cisloReceptuLabel.TabIndex = 3;
            this.cisloReceptuLabel.Text = "label5";
            this.cisloReceptuLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(5, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 16);
            this.label4.TabIndex = 2;
            this.label4.Text = "Pro rozměr:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(5, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 16);
            this.label3.TabIndex = 1;
            this.label3.Text = "Polotovar:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(5, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 16);
            this.label2.TabIndex = 0;
            this.label2.Text = "Zvolený recept:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox70);
            this.groupBox5.Controls.Add(this.groupBox120);
            this.groupBox5.Controls.Add(this.barZnacTiskLabel);
            this.groupBox5.Controls.Add(this.groupBox150);
            this.groupBox5.Controls.Add(this.groupBox220);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox5.Location = new System.Drawing.Point(432, 50);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(195, 441);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Čísla dávek:";
            // 
            // groupBox70
            // 
            this.groupBox70.Controls.Add(this.label24);
            this.groupBox70.Controls.Add(this.smes4NumericUpDown2);
            this.groupBox70.Controls.Add(this.smes4NumericUpDown1);
            this.groupBox70.Controls.Add(this.label19);
            this.groupBox70.Location = new System.Drawing.Point(0, 302);
            this.groupBox70.Name = "groupBox70";
            this.groupBox70.Size = new System.Drawing.Size(195, 100);
            this.groupBox70.TabIndex = 6;
            this.groupBox70.TabStop = false;
            this.groupBox70.Text = "Ø 70 mm";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label24.Location = new System.Drawing.Point(6, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(183, 24);
            this.label24.TabIndex = 43;
            this.label24.Text = "Extruder - spojovací poduška ";
            // 
            // smes4NumericUpDown2
            // 
            this.smes4NumericUpDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes4NumericUpDown2.Location = new System.Drawing.Point(101, 46);
            this.smes4NumericUpDown2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes4NumericUpDown2.Name = "smes4NumericUpDown2";
            this.smes4NumericUpDown2.Size = new System.Drawing.Size(70, 21);
            this.smes4NumericUpDown2.TabIndex = 18;
            this.smes4NumericUpDown2.Enter += new System.EventHandler(this.smes4NumericUpDown2_Enter);
            // 
            // smes4NumericUpDown1
            // 
            this.smes4NumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes4NumericUpDown1.Location = new System.Drawing.Point(6, 46);
            this.smes4NumericUpDown1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes4NumericUpDown1.Name = "smes4NumericUpDown1";
            this.smes4NumericUpDown1.Size = new System.Drawing.Size(70, 21);
            this.smes4NumericUpDown1.TabIndex = 17;
            this.smes4NumericUpDown1.Enter += new System.EventHandler(this.smes4NumericUpDown1_Enter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(84, 49);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 15);
            this.label19.TabIndex = 38;
            this.label19.Text = "-";
            // 
            // groupBox120
            // 
            this.groupBox120.Controls.Add(this.label28);
            this.groupBox120.Controls.Add(this.smes3NumericUpDown3);
            this.groupBox120.Controls.Add(this.smes3NumericUpDown2);
            this.groupBox120.Controls.Add(this.smes3NumericUpDown4);
            this.groupBox120.Controls.Add(this.smes3NumericUpDown1);
            this.groupBox120.Controls.Add(this.label27);
            this.groupBox120.Location = new System.Drawing.Point(0, 225);
            this.groupBox120.Name = "groupBox120";
            this.groupBox120.Size = new System.Drawing.Size(195, 76);
            this.groupBox120.TabIndex = 5;
            this.groupBox120.TabStop = false;
            this.groupBox120.Text = "Ø 120 mm";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label28.Location = new System.Drawing.Point(89, 29);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(11, 15);
            this.label28.TabIndex = 59;
            this.label28.Text = "-";
            // 
            // smes3NumericUpDown3
            // 
            this.smes3NumericUpDown3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes3NumericUpDown3.Location = new System.Drawing.Point(10, 51);
            this.smes3NumericUpDown3.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes3NumericUpDown3.Name = "smes3NumericUpDown3";
            this.smes3NumericUpDown3.Size = new System.Drawing.Size(70, 21);
            this.smes3NumericUpDown3.TabIndex = 15;
            this.smes3NumericUpDown3.Enter += new System.EventHandler(this.smes3NumericUpDown3_Enter);
            // 
            // smes3NumericUpDown2
            // 
            this.smes3NumericUpDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes3NumericUpDown2.Location = new System.Drawing.Point(106, 26);
            this.smes3NumericUpDown2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes3NumericUpDown2.Name = "smes3NumericUpDown2";
            this.smes3NumericUpDown2.Size = new System.Drawing.Size(70, 21);
            this.smes3NumericUpDown2.TabIndex = 14;
            this.smes3NumericUpDown2.Enter += new System.EventHandler(this.smes3NumericUpDown2_Enter);
            // 
            // smes3NumericUpDown4
            // 
            this.smes3NumericUpDown4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes3NumericUpDown4.Location = new System.Drawing.Point(106, 51);
            this.smes3NumericUpDown4.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes3NumericUpDown4.Name = "smes3NumericUpDown4";
            this.smes3NumericUpDown4.Size = new System.Drawing.Size(70, 21);
            this.smes3NumericUpDown4.TabIndex = 16;
            this.smes3NumericUpDown4.Enter += new System.EventHandler(this.smes3NumericUpDown4_Enter);
            // 
            // smes3NumericUpDown1
            // 
            this.smes3NumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes3NumericUpDown1.Location = new System.Drawing.Point(10, 26);
            this.smes3NumericUpDown1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes3NumericUpDown1.Name = "smes3NumericUpDown1";
            this.smes3NumericUpDown1.Size = new System.Drawing.Size(70, 21);
            this.smes3NumericUpDown1.TabIndex = 13;
            this.smes3NumericUpDown1.Enter += new System.EventHandler(this.smes3NumericUpDown1_Enter);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label27.Location = new System.Drawing.Point(89, 54);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(11, 15);
            this.label27.TabIndex = 62;
            this.label27.Text = "-";
            // 
            // groupBox150
            // 
            this.groupBox150.Controls.Add(this.smes2NumericUpDown1);
            this.groupBox150.Controls.Add(this.label18);
            this.groupBox150.Controls.Add(this.smes2NumericUpDown2);
            this.groupBox150.Controls.Add(this.label25);
            this.groupBox150.Controls.Add(this.smes2NumericUpDown4);
            this.groupBox150.Controls.Add(this.smes2NumericUpDown3);
            this.groupBox150.Location = new System.Drawing.Point(0, 146);
            this.groupBox150.Name = "groupBox150";
            this.groupBox150.Size = new System.Drawing.Size(195, 78);
            this.groupBox150.TabIndex = 4;
            this.groupBox150.TabStop = false;
            this.groupBox150.Text = "Ø 150 mm";
            // 
            // smes2NumericUpDown1
            // 
            this.smes2NumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes2NumericUpDown1.Location = new System.Drawing.Point(8, 27);
            this.smes2NumericUpDown1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes2NumericUpDown1.Name = "smes2NumericUpDown1";
            this.smes2NumericUpDown1.Size = new System.Drawing.Size(70, 21);
            this.smes2NumericUpDown1.TabIndex = 9;
            this.smes2NumericUpDown1.Enter += new System.EventHandler(this.smes2NumericUpDown1_Enter);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(87, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(11, 15);
            this.label18.TabIndex = 37;
            this.label18.Text = "-";
            // 
            // smes2NumericUpDown2
            // 
            this.smes2NumericUpDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes2NumericUpDown2.Location = new System.Drawing.Point(104, 27);
            this.smes2NumericUpDown2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes2NumericUpDown2.Name = "smes2NumericUpDown2";
            this.smes2NumericUpDown2.Size = new System.Drawing.Size(70, 21);
            this.smes2NumericUpDown2.TabIndex = 10;
            this.smes2NumericUpDown2.Enter += new System.EventHandler(this.smes2NumericUpDown2_Enter);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label25.Location = new System.Drawing.Point(87, 55);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(11, 15);
            this.label25.TabIndex = 55;
            this.label25.Text = "-";
            // 
            // smes2NumericUpDown4
            // 
            this.smes2NumericUpDown4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes2NumericUpDown4.Location = new System.Drawing.Point(104, 52);
            this.smes2NumericUpDown4.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes2NumericUpDown4.Name = "smes2NumericUpDown4";
            this.smes2NumericUpDown4.Size = new System.Drawing.Size(70, 21);
            this.smes2NumericUpDown4.TabIndex = 12;
            this.smes2NumericUpDown4.Enter += new System.EventHandler(this.smes2NumericUpDown4_Enter);
            // 
            // smes2NumericUpDown3
            // 
            this.smes2NumericUpDown3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes2NumericUpDown3.Location = new System.Drawing.Point(8, 52);
            this.smes2NumericUpDown3.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes2NumericUpDown3.Name = "smes2NumericUpDown3";
            this.smes2NumericUpDown3.Size = new System.Drawing.Size(70, 21);
            this.smes2NumericUpDown3.TabIndex = 11;
            this.smes2NumericUpDown3.Enter += new System.EventHandler(this.smes2NumericUpDown3_Enter);
            // 
            // groupBox220
            // 
            this.groupBox220.Controls.Add(this.label6);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown2);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown1);
            this.groupBox220.Controls.Add(this.label1);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown4);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown3);
            this.groupBox220.Controls.Add(this.label20);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown6);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown5);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown7);
            this.groupBox220.Controls.Add(this.label21);
            this.groupBox220.Controls.Add(this.smes1NumericUpDown8);
            this.groupBox220.Location = new System.Drawing.Point(0, 20);
            this.groupBox220.Name = "groupBox220";
            this.groupBox220.Size = new System.Drawing.Size(195, 125);
            this.groupBox220.TabIndex = 3;
            this.groupBox220.TabStop = false;
            this.groupBox220.Text = "Ø 200 mm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(87, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 15);
            this.label6.TabIndex = 31;
            this.label6.Text = "-";
            // 
            // smes1NumericUpDown2
            // 
            this.smes1NumericUpDown2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown2.Location = new System.Drawing.Point(104, 22);
            this.smes1NumericUpDown2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown2.Name = "smes1NumericUpDown2";
            this.smes1NumericUpDown2.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown2.TabIndex = 2;
            this.smes1NumericUpDown2.Enter += new System.EventHandler(this.smes1NumericUpDown2_Enter);
            // 
            // smes1NumericUpDown1
            // 
            this.smes1NumericUpDown1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown1.Location = new System.Drawing.Point(9, 22);
            this.smes1NumericUpDown1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown1.Name = "smes1NumericUpDown1";
            this.smes1NumericUpDown1.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown1.TabIndex = 1;
            this.smes1NumericUpDown1.Enter += new System.EventHandler(this.smes1NumericUpDown1_Enter);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(87, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 15);
            this.label1.TabIndex = 46;
            this.label1.Text = "-";
            // 
            // smes1NumericUpDown4
            // 
            this.smes1NumericUpDown4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown4.Location = new System.Drawing.Point(104, 47);
            this.smes1NumericUpDown4.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown4.Name = "smes1NumericUpDown4";
            this.smes1NumericUpDown4.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown4.TabIndex = 5;
            this.smes1NumericUpDown4.Enter += new System.EventHandler(this.smes1NumericUpDown4_Enter);
            // 
            // smes1NumericUpDown3
            // 
            this.smes1NumericUpDown3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown3.Location = new System.Drawing.Point(9, 47);
            this.smes1NumericUpDown3.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown3.Name = "smes1NumericUpDown3";
            this.smes1NumericUpDown3.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown3.TabIndex = 4;
            this.smes1NumericUpDown3.Enter += new System.EventHandler(this.smes1NumericUpDown3_Enter);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label20.Location = new System.Drawing.Point(86, 75);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(11, 15);
            this.label20.TabIndex = 49;
            this.label20.Text = "-";
            // 
            // smes1NumericUpDown6
            // 
            this.smes1NumericUpDown6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown6.Location = new System.Drawing.Point(103, 72);
            this.smes1NumericUpDown6.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown6.Name = "smes1NumericUpDown6";
            this.smes1NumericUpDown6.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown6.TabIndex = 7;
            this.smes1NumericUpDown6.Enter += new System.EventHandler(this.smes1NumericUpDown6_Enter);
            // 
            // smes1NumericUpDown5
            // 
            this.smes1NumericUpDown5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown5.Location = new System.Drawing.Point(8, 72);
            this.smes1NumericUpDown5.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown5.Name = "smes1NumericUpDown5";
            this.smes1NumericUpDown5.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown5.TabIndex = 6;
            this.smes1NumericUpDown5.Enter += new System.EventHandler(this.smes1NumericUpDown5_Enter);
            // 
            // smes1NumericUpDown7
            // 
            this.smes1NumericUpDown7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown7.Location = new System.Drawing.Point(8, 97);
            this.smes1NumericUpDown7.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown7.Name = "smes1NumericUpDown7";
            this.smes1NumericUpDown7.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown7.TabIndex = 8;
            this.smes1NumericUpDown7.Enter += new System.EventHandler(this.smes1NumericUpDown7_Enter);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.Location = new System.Drawing.Point(86, 100);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 15);
            this.label21.TabIndex = 52;
            this.label21.Text = "-";
            // 
            // smes1NumericUpDown8
            // 
            this.smes1NumericUpDown8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smes1NumericUpDown8.Location = new System.Drawing.Point(103, 97);
            this.smes1NumericUpDown8.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.smes1NumericUpDown8.Name = "smes1NumericUpDown8";
            this.smes1NumericUpDown8.Size = new System.Drawing.Size(70, 21);
            this.smes1NumericUpDown8.TabIndex = 9;
            this.smes1NumericUpDown8.Enter += new System.EventHandler(this.smes1NumericUpDown8_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.smenyComboBox);
            this.groupBox3.Controls.Add(this.obsluhaComboBox);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(6, 42);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(250, 96);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Osádka, směna:";
            // 
            // smenyComboBox
            // 
            this.smenyComboBox.Enabled = false;
            this.smenyComboBox.FormattingEnabled = true;
            this.smenyComboBox.Location = new System.Drawing.Point(0, 56);
            this.smenyComboBox.Name = "smenyComboBox";
            this.smenyComboBox.Size = new System.Drawing.Size(199, 28);
            this.smenyComboBox.TabIndex = 20;
            this.smenyComboBox.Text = "Vyber směnu";
            this.smenyComboBox.SelectedIndexChanged += new System.EventHandler(this.smenyComboBox_SelectedIndexChanged);
            // 
            // obsluhaComboBox
            // 
            this.obsluhaComboBox.Enabled = false;
            this.obsluhaComboBox.FormattingEnabled = true;
            this.obsluhaComboBox.Location = new System.Drawing.Point(0, 22);
            this.obsluhaComboBox.Name = "obsluhaComboBox";
            this.obsluhaComboBox.Size = new System.Drawing.Size(199, 28);
            this.obsluhaComboBox.TabIndex = 19;
            this.obsluhaComboBox.Text = "Vyber obsluhu";
            this.obsluhaComboBox.SelectedIndexChanged += new System.EventHandler(this.obsluhaComboBox_SelectedIndexChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(123, 423);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(142, 16);
            this.label13.TabIndex = 18;
            this.label13.Text = "Copyright (c) KaP 2017";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cisloPruvodkyLabel);
            this.groupBox4.Controls.Add(this.zpracDoLabel);
            this.groupBox4.Controls.Add(this.zpracOdLabel);
            this.groupBox4.Controls.Add(this.label13);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.vyrobilTextBox);
            this.groupBox4.Controls.Add(this.smesLabel2);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Controls.Add(this.proRozmeryLabel2);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.tiskButton1);
            this.groupBox4.Controls.Add(this.pruvodkaPolotovaruLabel);
            this.groupBox4.Controls.Add(this.cisloReceptuLabel2);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(633, 50);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(270, 441);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Náhled průvodky:";
            // 
            // cisloPruvodkyLabel
            // 
            this.cisloPruvodkyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloPruvodkyLabel.Location = new System.Drawing.Point(105, 361);
            this.cisloPruvodkyLabel.Name = "cisloPruvodkyLabel";
            this.cisloPruvodkyLabel.Size = new System.Drawing.Size(151, 20);
            this.cisloPruvodkyLabel.TabIndex = 30;
            this.cisloPruvodkyLabel.Text = "label5";
            this.cisloPruvodkyLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // zpracDoLabel
            // 
            this.zpracDoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zpracDoLabel.Location = new System.Drawing.Point(105, 341);
            this.zpracDoLabel.Name = "zpracDoLabel";
            this.zpracDoLabel.Size = new System.Drawing.Size(151, 20);
            this.zpracDoLabel.TabIndex = 29;
            this.zpracDoLabel.Text = "label5";
            this.zpracDoLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // zpracOdLabel
            // 
            this.zpracOdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.zpracOdLabel.Location = new System.Drawing.Point(108, 321);
            this.zpracOdLabel.Name = "zpracOdLabel";
            this.zpracOdLabel.Size = new System.Drawing.Size(148, 20);
            this.zpracOdLabel.TabIndex = 28;
            this.zpracOdLabel.Text = "label5";
            this.zpracOdLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(6, 361);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 16);
            this.label17.TabIndex = 27;
            this.label17.Text = "Číslo průvodky:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(6, 321);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 16);
            this.label16.TabIndex = 26;
            this.label16.Text = "Zpracovat od: ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(6, 341);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 16);
            this.label15.TabIndex = 25;
            this.label15.Text = "Zpracovat do:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(6, 271);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(100, 16);
            this.label12.TabIndex = 24;
            this.label12.Text = "Vyrobil, směna:";
            // 
            // vyrobilTextBox
            // 
            this.vyrobilTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.vyrobilTextBox.Cursor = System.Windows.Forms.Cursors.No;
            this.vyrobilTextBox.Location = new System.Drawing.Point(6, 291);
            this.vyrobilTextBox.Name = "vyrobilTextBox";
            this.vyrobilTextBox.ReadOnly = true;
            this.vyrobilTextBox.Size = new System.Drawing.Size(250, 26);
            this.vyrobilTextBox.TabIndex = 23;
            this.vyrobilTextBox.TabStop = false;
            // 
            // smesLabel2
            // 
            this.smesLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.smesLabel2.Location = new System.Drawing.Point(58, 251);
            this.smesLabel2.Name = "smesLabel2";
            this.smesLabel2.Size = new System.Drawing.Size(198, 20);
            this.smesLabel2.TabIndex = 22;
            this.smesLabel2.Text = "label5";
            this.smesLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(6, 251);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 16);
            this.label14.TabIndex = 21;
            this.label14.Text = "Směs:";
            // 
            // proRozmeryLabel2
            // 
            this.proRozmeryLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.proRozmeryLabel2.Location = new System.Drawing.Point(21, 191);
            this.proRozmeryLabel2.Name = "proRozmeryLabel2";
            this.proRozmeryLabel2.Size = new System.Drawing.Size(235, 60);
            this.proRozmeryLabel2.TabIndex = 18;
            this.proRozmeryLabel2.Text = "label5";
            this.proRozmeryLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(6, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(58, 16);
            this.label10.TabIndex = 17;
            this.label10.Text = "Rozměr:";
            // 
            // tiskButton1
            // 
            this.tiskButton1.Enabled = false;
            this.tiskButton1.Location = new System.Drawing.Point(61, 382);
            this.tiskButton1.Name = "tiskButton1";
            this.tiskButton1.Size = new System.Drawing.Size(138, 42);
            this.tiskButton1.TabIndex = 12;
            this.tiskButton1.Text = "Tisk průvodky";
            this.tiskButton1.UseVisualStyleBackColor = true;
            this.tiskButton1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pruvodkaPolotovaruLabel
            // 
            this.pruvodkaPolotovaruLabel.AutoSize = true;
            this.pruvodkaPolotovaruLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pruvodkaPolotovaruLabel.Location = new System.Drawing.Point(6, 22);
            this.pruvodkaPolotovaruLabel.Name = "pruvodkaPolotovaruLabel";
            this.pruvodkaPolotovaruLabel.Size = new System.Drawing.Size(156, 16);
            this.pruvodkaPolotovaruLabel.TabIndex = 15;
            this.pruvodkaPolotovaruLabel.Text = "Průvodka polotovaru:";
            // 
            // cisloReceptuLabel2
            // 
            this.cisloReceptuLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloReceptuLabel2.Location = new System.Drawing.Point(156, 146);
            this.cisloReceptuLabel2.Name = "cisloReceptuLabel2";
            this.cisloReceptuLabel2.Size = new System.Drawing.Size(100, 20);
            this.cisloReceptuLabel2.TabIndex = 14;
            this.cisloReceptuLabel2.Text = "label5";
            this.cisloReceptuLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(6, 142);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(100, 16);
            this.label9.TabIndex = 13;
            this.label9.Text = "Zvolený recept:";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument1;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // pageSetupDialog1
            // 
            this.pageSetupDialog1.AllowOrientation = false;
            this.pageSetupDialog1.AllowPrinter = false;
            this.pageSetupDialog1.Document = this.printDocument1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem,
            this.zobrazitToolStripMenuItem,
            this.nápovědaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(3, 3);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(902, 24);
            this.menuStrip1.TabIndex = 23;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // souborToolStripMenuItem
            // 
            this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.seznamPrůvodekToolStripMenuItem,
            this.konecToolStripMenuItem});
            this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
            this.souborToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.souborToolStripMenuItem.Text = "Soubor";
            // 
            // seznamPrůvodekToolStripMenuItem
            // 
            this.seznamPrůvodekToolStripMenuItem.Name = "seznamPrůvodekToolStripMenuItem";
            this.seznamPrůvodekToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.seznamPrůvodekToolStripMenuItem.Text = "Seznam průvodek";
            this.seznamPrůvodekToolStripMenuItem.Click += new System.EventHandler(this.seznamPrůvodekToolStripMenuItem_Click);
            // 
            // konecToolStripMenuItem
            // 
            this.konecToolStripMenuItem.Name = "konecToolStripMenuItem";
            this.konecToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.konecToolStripMenuItem.Text = "Konec";
            this.konecToolStripMenuItem.Click += new System.EventHandler(this.konecToolStripMenuItem_Click);
            // 
            // zobrazitToolStripMenuItem
            // 
            this.zobrazitToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nastaveníToolStripMenuItem});
            this.zobrazitToolStripMenuItem.Name = "zobrazitToolStripMenuItem";
            this.zobrazitToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.zobrazitToolStripMenuItem.Text = "Zobrazit";
            // 
            // nastaveníToolStripMenuItem
            // 
            this.nastaveníToolStripMenuItem.Name = "nastaveníToolStripMenuItem";
            this.nastaveníToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.nastaveníToolStripMenuItem.Text = "Nastavení";
            this.nastaveníToolStripMenuItem.Click += new System.EventHandler(this.nastaveníToolStripMenuItem_Click);
            // 
            // nápovědaToolStripMenuItem
            // 
            this.nápovědaToolStripMenuItem.Name = "nápovědaToolStripMenuItem";
            this.nápovědaToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.nápovědaToolStripMenuItem.Text = "Nápověda";
            // 
            // barZnacTiskLabel
            // 
            this.barZnacTiskLabel.AutoSize = true;
            this.barZnacTiskLabel.Location = new System.Drawing.Point(66, 417);
            this.barZnacTiskLabel.Name = "barZnacTiskLabel";
            this.barZnacTiskLabel.Size = new System.Drawing.Size(135, 20);
            this.barZnacTiskLabel.TabIndex = 20;
            this.barZnacTiskLabel.Text = "barZnacTiskLabel";
            this.barZnacTiskLabel.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 30);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 13);
            this.label11.TabIndex = 24;
            this.label11.Text = "Aktuální čas:";
            // 
            // hlavniOknoTriplexForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(908, 492);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.nyniLabel);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "hlavniOknoTriplexForm";
            this.Padding = new System.Windows.Forms.Padding(3);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kapa pro Triplex";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox70.ResumeLayout(false);
            this.groupBox70.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes4NumericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes4NumericUpDown1)).EndInit();
            this.groupBox120.ResumeLayout(false);
            this.groupBox120.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes3NumericUpDown1)).EndInit();
            this.groupBox150.ResumeLayout(false);
            this.groupBox150.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes2NumericUpDown3)).EndInit();
            this.groupBox220.ResumeLayout(false);
            this.groupBox220.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.smes1NumericUpDown8)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label nyniLabel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox seznamReceptuListBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label datumPoslAktLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label smesLabel;
        private System.Windows.Forms.Label proRozmeryLabel;
        private System.Windows.Forms.Label barevneZnaceniLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label polotovarLabel;
        private System.Windows.Forms.Label cisloReceptuLabel;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox smenyComboBox;
        private System.Windows.Forms.ComboBox obsluhaComboBox;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label cisloReceptuLabel2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label pruvodkaPolotovaruLabel;
        private System.Windows.Forms.Label proRozmeryLabel2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button tiskButton1;
        private System.Windows.Forms.Label smesLabel2;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox vyrobilTextBox;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.Label zpracDoLabel;
        private System.Windows.Forms.Label zpracOdLabel;
        public System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown smes4NumericUpDown2;
        private System.Windows.Forms.NumericUpDown smes4NumericUpDown1;
        private System.Windows.Forms.NumericUpDown smes2NumericUpDown2;
        private System.Windows.Forms.NumericUpDown smes2NumericUpDown1;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem seznamPrůvodekToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem konecToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nápovědaToolStripMenuItem;
        private System.Windows.Forms.Label cisloPruvodkyLabel;
        private System.Windows.Forms.Label barZnacTiskLabel;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ToolStripMenuItem zobrazitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nastaveníToolStripMenuItem;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown7;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown8;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown5;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown3;
        private System.Windows.Forms.NumericUpDown smes1NumericUpDown4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown smes3NumericUpDown3;
        private System.Windows.Forms.NumericUpDown smes3NumericUpDown4;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown smes3NumericUpDown1;
        private System.Windows.Forms.NumericUpDown smes3NumericUpDown2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown smes2NumericUpDown3;
        private System.Windows.Forms.NumericUpDown smes2NumericUpDown4;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox150;
        private System.Windows.Forms.GroupBox groupBox220;
        private System.Windows.Forms.GroupBox groupBox70;
        private System.Windows.Forms.GroupBox groupBox120;
    }
}