﻿using Kapa_pro_triplex;
using System;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Kapa_pro_triplex
{
    public partial class hlavniOknoTriplexForm : Form
    {
        private Databaze databaze;
        private DatabazePruvodek databazePruvodek;


        public hlavniOknoTriplexForm()
        {
            //vymazání labelů při počátečním načtení formuláře
            #region inicializace okna a vymazání labelů
            InitializeComponent();
            cisloReceptuLabel.Text = "";
            polotovarLabel.Text = "";
            proRozmeryLabel.Text = "";
            smesLabel.Text = "";
            barevneZnaceniLabel.Text = "";
            datumPoslAktLabel.Text = "";
            //u pruvodky
            cisloReceptuLabel2.Text = "";
            proRozmeryLabel2.Text = "";
            smesLabel2.Text = "";
            zpracOdLabel.Text = "";
            zpracDoLabel.Text = "";
            #endregion
            //vytvoření složky AppData\kapa, nastavení cesty, try, catch
            //cesta do AppData
            #region cesta do AppData
            string cesta = "";
            try
            {
                cesta = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "kapa_tri");
                if (!Directory.Exists(cesta))
                    Directory.CreateDirectory(cesta);

            }
            catch
            {
                Console.WriteLine("Nepodařilo se vytvořit složku {0}, zkontrolujte prosím svá oprávnění.", cesta);
            }
            #endregion
            //zkopírování sobourů do složky AppData\kapa
            #region kopírování souborů
            try
            {

                //recepty
                if (!File.Exists(cesta + "\\" + "recepty.csv"))
                {
                    File.Copy(Application.StartupPath + "\\" + "recepty.csv", cesta + "\\" + "recepty.csv");
                }
                //obsluha
                if (!File.Exists(cesta + "\\" + "obsluha.txt"))
                {
                    File.Copy(Application.StartupPath + "\\" + "obsluha.txt", cesta + "\\" + "obsluha.txt");
                }
                //smeny
                if (!File.Exists(cesta + "\\" + "smeny.txt"))
                {
                    File.Copy(Application.StartupPath + "\\" + "smeny.txt", cesta + "\\" + "smeny.txt");
                }
                if (!File.Exists(cesta + "\\" + "pruvodky.csv"))
                {
                    File.Copy(Application.StartupPath + "\\" + "pruvodky.csv", cesta + "\\" + "pruvodky.csv");
                }

            }

            catch
            {
                Console.WriteLine("Chyba kopírování souborů.");
            }
            #endregion
            #region načtení comboboxů
            //načtení souboru s obsluhou
            string[] obsluha = File.ReadAllLines(cesta + "\\" + "obsluha.txt", Encoding.Default);
            foreach (var line in obsluha)
            {
                try
                {
                    string[] radky = line.Split(' ');
                    obsluhaComboBox.Items.Add(radky[0]);
                }
                catch (Exception)
                {
                    Console.WriteLine("Chyba načtení souboru obsluha.txt.");
                }
            }
            //načtení souboru se směnami
            string[] smeny = File.ReadAllLines(cesta + "\\" + "smeny.txt", Encoding.Default);
            foreach (var line in smeny)
            {
                try
                {
                    string[] radky = line.Split(' ');
                    smenyComboBox.Items.Add(radky[0]);
                }
                catch (Exception)
                {
                    Console.WriteLine("Chyba načtení souboru smeny.txt.");
                }
            }
            #endregion
            #region načtení databaze (seznamu receptů)
            //načtení receptů ze souboru recepty.csv, vytvoření nové instance třídy databáze
            databaze = new Databaze(cesta + "\\" + "recepty.csv"); //přidání zpětných lomítek!
            try
            {
                databaze.Nacti();
                seznamReceptuListBox.Items.Clear();
                seznamReceptuListBox.Items.AddRange(databaze.VratVsechny());
            }
            catch
            {
                MessageBox.Show("Databázi se nepodařilo načíst, soubor zřejmě neexistuje nebo je otevřen jinou aplikací.",
                        "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
            #region nacteni databaze pruvodek
            databazePruvodek = new DatabazePruvodek(cesta + "\\" + "pruvodky.csv");
            try
            {
                databazePruvodek.NactiCisloPosledniPruvodky();
                int j;
                int.TryParse(databazePruvodek.cisloPosledniPruvodky, out j);
                int poslPruv = j + 1;
                string posledniPruvodka = poslPruv.ToString();
                cisloPruvodkyLabel.Text = posledniPruvodka;
            }
            catch 
            {

                MessageBox.Show("Databázi se nepodařilo načíst, soubor zřejmě neexistuje nebo je otevřen jinou aplikací.",
                       "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
            

        }// tady končí hlavniOknoTriplexForm
        string smes1;
        string smes2;
        string smes3;
        string smes4;
        private void seznamReceptuListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (seznamReceptuListBox.SelectedItem != null)
            {
                Recept vybrany = (Recept)seznamReceptuListBox.SelectedItem;
                cisloReceptuLabel.Text = vybrany.CisloReceptu;
                cisloReceptuLabel2.Text = vybrany.CisloReceptu;
                polotovarLabel.Text = vybrany.Polotovar;
                pruvodkaPolotovaruLabel.Text = "Průvodka polotovaru: " +vybrany.Polotovar;
                proRozmeryLabel.Text = vybrany.Rozmer;
                proRozmeryLabel2.Text = vybrany.Rozmer;
                #region zobrazení okýnek pro zadávání směsí
                if (vybrany.Smes220 != "")
                {
                    smes1NumericUpDown1.Enabled = true;
                    smes1NumericUpDown2.Enabled = true;
                    smes1NumericUpDown3.Enabled = true;
                    smes1NumericUpDown4.Enabled = true;
                    smes1NumericUpDown5.Enabled = true;
                    smes1NumericUpDown6.Enabled = true;
                    smes1NumericUpDown7.Enabled = true;
                    smes1NumericUpDown8.Enabled = true;
                    
                }
                else
                {
                    smes1NumericUpDown1.Enabled = false;
                    smes1NumericUpDown2.Enabled = false;
                    smes1NumericUpDown3.Enabled = false;
                    smes1NumericUpDown4.Enabled = false;
                    smes1NumericUpDown5.Enabled = false;
                    smes1NumericUpDown6.Enabled = false;
                    smes1NumericUpDown7.Enabled = false;
                    smes1NumericUpDown8.Enabled = false;
                    
                }
                if (vybrany.Smes150 != "")
                {
                    smes2NumericUpDown1.Enabled = true;
                    smes2NumericUpDown2.Enabled = true;
                    smes2NumericUpDown3.Enabled = true;
                    smes2NumericUpDown4.Enabled = true;

                }
                else
                {
                    smes2NumericUpDown1.Enabled = false;
                    smes2NumericUpDown2.Enabled = false;
                    smes2NumericUpDown3.Enabled = false;
                    smes2NumericUpDown4.Enabled = false;
                }
                if (vybrany.Smes120 != "")
                {
                    smes3NumericUpDown1.Enabled = true;
                    smes3NumericUpDown2.Enabled = true;
                    smes3NumericUpDown3.Enabled = true;
                    smes3NumericUpDown4.Enabled = true;
                }
                else
                {
                    smes3NumericUpDown1.Enabled = false;
                    smes3NumericUpDown2.Enabled = false;
                    smes3NumericUpDown3.Enabled = false;
                    smes3NumericUpDown4.Enabled = false;
                }
                if (vybrany.Smes70 != "")
                {
                    smes4NumericUpDown1.Enabled = true;
                    smes4NumericUpDown2.Enabled = true;
                }
                else
                {
                    smes4NumericUpDown1.Enabled = false;
                    smes4NumericUpDown2.Enabled = false;
                }
                #endregion

                //pridat podminky pro zobrazeni řádku se směsí

                string smes1a = "";
                string smes2a = "";

                if (vybrany.Smes220!="")
                {
                    smes1 = vybrany.Smes220 ;
                    smes1a = smes1 + "/";
                }
                else
                {
                    smes1 = "";
                }
                if (vybrany.Smes150 != "")
                {
                    smes2 = vybrany.Smes150;
                    smes2a = smes2 + "/";
                }
                else
                {
                    smes2 = "";
                }
                if (vybrany.Smes120 != "")
                {
                    smes3 = vybrany.Smes120;
                }
                else
                {
                    smes3 = "";
                }
                if (vybrany.Smes70 != "")
                {
                    smes4 = vybrany.Smes70;
                }
                else
                {
                    smes4 = "";
                }
                if (true)
                {

                }

               
               

                smesLabel.Text = smes1a+smes2a+smes3 +smes4;
                smesLabel2.Text = smesLabel.Text;
                barevneZnaceniLabel.Text = vybrany.BarZnac1+" | "+vybrany.BarZnac2 + " | " + vybrany.BarZnac3 + " | " + vybrany.BarZnac4 + " |";
                datumPoslAktLabel.Text = vybrany.DatumAkt ;
                barZnacTiskLabel.Text= vybrany.BarZnac1 + " " + vybrany.BarZnac2 + " " + vybrany.BarZnac3 + " " + vybrany.BarZnac4;

            }
            obsluhaComboBox.Enabled = true;
                    }

        //obsluha
        private void obsluhaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (smenyComboBox.SelectedItem!=null)
            {
                vyrobilTextBox.Text = obsluhaComboBox.SelectedItem.ToString() + ", " + smenyComboBox.SelectedItem.ToString(); ;
            }
            else
            {
                vyrobilTextBox.Text = obsluhaComboBox.SelectedItem.ToString();
            }
            
            smenyComboBox.Enabled = true;
        }

        private void smenyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            vyrobilTextBox.Text = obsluhaComboBox.SelectedItem.ToString() + ", " + smenyComboBox.SelectedItem.ToString();
            tiskButton1.Enabled = true;
        }
        //časovač pro čas a datum zpracování
        private void timer1_Tick(object sender, EventArgs e)
        {
            nyniLabel.Text = DateTime.Now.ToString();
            zpracOdLabel.Text = Convert.ToString(DateTime.Now.AddHours(4));
            zpracDoLabel.Text = Convert.ToString(DateTime.Now.AddHours(360));
        }
        
        //TISK
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //počátek tisku
            int pocatek_x=0;
            int pocatek_y=0;
            #region definice pro tisk - rámečky, fonty - pro všechny průvodky
            Graphics g = e.Graphics;
            //rámečky na průvodku
            Rectangle r = new Rectangle(0, 0, 0, 0); //nový rámeček
            Pen p = new Pen(Color.Black); //nové pero černé barvy
            g.DrawRectangle(p, 0, 0, 376, 524); //nakreslí rámeček čtvrtina A4 (=A6) 413x585
            //fonty
            //Arial 20
            Font messageFontArial_20 = new Font("Arial", 20, GraphicsUnit.Pixel);
            //Arial 12
            Font messageFontArial_12 = new Font("Arial", 12, GraphicsUnit.Pixel);
            //Arial 10
            Font messageFontArial_10 = new Font("Arial", 10, GraphicsUnit.Pixel);
            //Arial 9
            Font messageFontArial_9 = new Font("Arial", 9, GraphicsUnit.Pixel);
            //Arial 7
            Font messageFontArial_7 = new Font("Arial", 7, GraphicsUnit.Pixel);
            #endregion
            /*
            //rámečky na spodu nové průvodky - tabulka KONFEKCE z druhé strany staré průvodky
            Rectangle r11 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 415, 200, 37);
            Rectangle r12 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 452, 200, 37);
            Rectangle r13 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 489, 200, 37);
            Rectangle r14 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 526, 200, 37);
            Rectangle r21 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 415, 200, 37);
            Rectangle r22 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 452, 200, 37);
            Rectangle r23 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 489, 200, 37);
            Rectangle r24 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 526, 200, 37);
            //rámečky pro proměnné
            Rectangle r3 = new Rectangle(0, 0, 0, 0); //nový rámeček
            for (int i = 75; i < 320; i += 35)
            {
                g.DrawRectangle(p, 130, i, 275, 30);
            }
            Rectangle r4 = new Rectangle(0, 0, 0, 0); //rámeček recept
            g.DrawRectangle(p, 130, 40, 40, 30);
            */
            //Rectangle r5 = new Rectangle(0, 0, 0, 0); //rámeček barevné značení
            //g.DrawRectangle(p, 315, 40, 90, 30);

            //jednotlivé řádky průvodky
            //SYNTAXE TŘÍDY g.DrawString: (název_proměnné, FONT: messageFontArial_vetsi, ŠTĚTEC: Brushes.Black,X: 10,Y: 0);
            //první řádek
            //string message0 = "Průvodka polotovaru";
            //g.DrawString(message0, messageFontArial_20, Brushes.Black, 5, 5);
            //string message01 = "Recept: ";
            //g.DrawString(message01, messageFontArial_12, Brushes.Black, 5, 53);
            string message02 = cisloReceptuLabel.Text.ToString();      //x,   y
            g.DrawString(message02, messageFontArial_20, Brushes.Black, (pocatek_x+110), (pocatek_y+42));
            string message03 = "Barevné značení: ";
            g.DrawString(message03, messageFontArial_12, Brushes.Black, (pocatek_x + 160), (pocatek_y + 45));
            string message04 = barZnacTiskLabel.Text.ToString();
            g.DrawString(message04, messageFontArial_20, Brushes.Black, (pocatek_x + 260), (pocatek_y + 42));
            //druhý řádek
            //string message1 = "Polotovar:";
            //g.DrawString(message1, messageFontArial_12, Brushes.Black, 5, 88);
            string message11 = polotovarLabel.Text.ToString();
            g.DrawString(message11, messageFontArial_20, Brushes.Black, (pocatek_x + 110), (pocatek_y + 82));
            //třetí řádek
            //string message2 = "Pro rozměr:";
            //g.DrawString(message2, messageFontArial_12, Brushes.Black, 5, 123);
            string message21 = proRozmeryLabel2.Text.ToString();
            g.DrawString(message21, messageFontArial_20, Brushes.Black, (pocatek_x + 110), (pocatek_y + 122));
            //čtvrtý řádek
            //string message3 = "Číslo směsi:";
            //g.DrawString(message3, messageFontArial_12, Brushes.Black, 5, 158);
            string message31a = "/";
            string message31b = "/";
            string message31c = "/";
            string message311 = smes1.ToString();
            string message312 = smes2.ToString();
            string message313 = smes3.ToString();
            string message314 = smes4.ToString();
                
               // smesLabel.Text.ToString();
            g.DrawString(message31a, messageFontArial_20, Brushes.Black, (pocatek_x + 190), (pocatek_y + 162));
            g.DrawString(message31b, messageFontArial_20, Brushes.Black, (pocatek_x + 250), (pocatek_y + 162));
            g.DrawString(message31c, messageFontArial_20, Brushes.Black, (pocatek_x + 305), (pocatek_y + 162));
            g.DrawString(message311, messageFontArial_20, Brushes.Black, (pocatek_x + 110), (pocatek_y + 162));
            g.DrawString(message312, messageFontArial_20, Brushes.Black, (pocatek_x + 200), (pocatek_y + 162));
            g.DrawString(message313, messageFontArial_20, Brushes.Black, (pocatek_x + 260), (pocatek_y + 162));
            g.DrawString(message314, messageFontArial_20, Brushes.Black, (pocatek_x + 315), (pocatek_y + 162));

            //pátý řádek
            //string message4 = "Číslo dávky:";
            //g.DrawString(message4, messageFontArial_12, Brushes.Black, 5, 193);
            //šnek 220 - 1
            if (smes1NumericUpDown1.Enabled==true)
            {
                string message411 = smes1NumericUpDown1.Text.ToString() + "-" + smes1NumericUpDown2.Text.ToString();
                //šnek 220 - 2
                string message412 = smes1NumericUpDown3.Text.ToString() + "-" + smes1NumericUpDown4.Text.ToString();
                //šnek 220 - 3
                string message413 = smes1NumericUpDown5.Text.ToString() + "-" + smes1NumericUpDown6.Text.ToString();
                //šnek 220 - 4
                string message414 = smes1NumericUpDown7.Text.ToString() + "-" + smes1NumericUpDown8.Text.ToString();
                g.DrawString(message411, messageFontArial_10, Brushes.Black, (pocatek_x + 110), (pocatek_y + 200));
                g.DrawString(message412, messageFontArial_10, Brushes.Black, (pocatek_x + 150), (pocatek_y + 200));
                g.DrawString(message413, messageFontArial_10, Brushes.Black, (pocatek_x + 110), (pocatek_y + 215));
                g.DrawString(message414, messageFontArial_10, Brushes.Black, (pocatek_x + 150), (pocatek_y + 215));
            }
            //šnek 150 - směs 2
            string message420 = "/";
            g.DrawString(message420, messageFontArial_20, Brushes.Black, (pocatek_x + 190), (pocatek_y + 200));
            if (smes2NumericUpDown1.Enabled==true)
            {
                
                //šnek 150 - 1
                string message421 = smes2NumericUpDown1.Text.ToString() + "-" + smes2NumericUpDown2.Text.ToString();
                //šnek 150 - 2
                string message422 = smes2NumericUpDown3.Text.ToString() + "-" + smes2NumericUpDown4.Text.ToString();
                
                g.DrawString(message421, messageFontArial_10, Brushes.Black, (pocatek_x + 205), (pocatek_y + 200));
                g.DrawString(message422, messageFontArial_10, Brushes.Black, (pocatek_x + 205), (pocatek_y + 215));
            }

            //šnek 120 - směs 3
            string message430 = "/";
            g.DrawString(message430, messageFontArial_20, Brushes.Black, (pocatek_x + 250), (pocatek_y + 200));
            if (smes3NumericUpDown1.Enabled==true)
            {
               
                //šnek 120 - 1
                string message431 = smes3NumericUpDown1.Text.ToString() + "-" + smes3NumericUpDown2.Text.ToString();
                //šnek 120 - 2
                string message432 = smes3NumericUpDown3.Text.ToString() + "-" + smes3NumericUpDown4.Text.ToString();
               
                g.DrawString(message431, messageFontArial_10, Brushes.Black, (pocatek_x + 265), (pocatek_y + 200));
                g.DrawString(message432, messageFontArial_10, Brushes.Black, (pocatek_x + 265), (pocatek_y + 215));
            }
            //šnek 70 - směs 4
            string message440 = "/";
            g.DrawString(message440, messageFontArial_20, Brushes.Black, (pocatek_x + 305), (pocatek_y + 200));
            if (smes4NumericUpDown1.Enabled==true)
            {
                
                //šnek 70 - 1
                string message441 = smes4NumericUpDown1.Text.ToString() + "-" + smes4NumericUpDown2.Text.ToString();
                
                g.DrawString(message441, messageFontArial_10, Brushes.Black, (pocatek_x + 315), (pocatek_y + 200));
            }
           
           

            //šestý řádek 
            string message5 = nyniLabel.Text.ToString(); //dat. / hod.
            g.DrawString(message5, messageFontArial_20, Brushes.Black, (pocatek_x + 110), (pocatek_y + 242));
            //string message51 = "Zpracuj do:";
            //g.DrawString(message51, messageFontArial_12, Brushes.Black, 5, 263);

            //sedmý řádek 
            //string message7 = "Vyrobil:";
            //g.DrawString(message7, messageFontArial_12, Brushes.Black, 5, 298);
            string message6 = obsluhaComboBox.SelectedItem.ToString() + ", " + smenyComboBox.SelectedItem.ToString() + " směna";
            g.DrawString(message6, messageFontArial_20, Brushes.Black, (pocatek_x + 110), (pocatek_y + 282));

            //osmý řádek - datum s časem
            string message7 = zpracOdLabel.Text.ToString();
            g.DrawString(message7, messageFontArial_20, Brushes.Black, (pocatek_x + 110), (pocatek_y + 368));
            string message71 = zpracDoLabel.Text.ToString();
            g.DrawString(message71, messageFontArial_20, Brushes.Black, (pocatek_x + 110), (pocatek_y + 408));

            
            
            #region nová průvodka
            /*
            Graphics g = e.Graphics;

            //rámečky na průvodku
            Rectangle r = new Rectangle(0, 0, 0, 0); //nový rámeček
            Pen p = new Pen(Color.Black); //nové pero černé barvy
            g.DrawRectangle(p, 0, 0, 413, 585); //nakreslí rámeček čtvrtina A4 (=A6) 


            //rámečky KONFEKCE
            Rectangle r11 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 415, 200, 37);
            Rectangle r12 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 452, 200, 37);
            Rectangle r13 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 489, 200, 37);
            Rectangle r14 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 5, 526, 200, 37);
            Rectangle r21 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 415, 200, 37);
            Rectangle r22 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 452, 200, 37);
            Rectangle r23 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 489, 200, 37);
            Rectangle r24 = new Rectangle(0, 0, 0, 0);
            g.DrawRectangle(p, 205, 526, 200, 37);

            //rámečky pro proměnné
            Rectangle r3 = new Rectangle(0, 0, 0, 0); //nový rámeček
            for (int i = 75; i < 320; i += 35)
            {
                g.DrawRectangle(p, 130, i, 275, 30);
            }
            Rectangle r4 = new Rectangle(0, 0, 0, 0); //rámeček recept
            g.DrawRectangle(p, 130, 40, 40, 30);
            Rectangle r5 = new Rectangle(0, 0, 0, 0); //rámeček barevné značení
            g.DrawRectangle(p, 315, 40, 90, 30);

            //fonty
            //Arial 20
            Font messageFontArial_20 = new Font("Arial", 20, GraphicsUnit.Pixel);
            //Arial 12
            Font messageFontArial_12 = new Font("Arial", 12, GraphicsUnit.Pixel);
            //Arial 10
            Font messageFontArial_10 = new Font("Arial", 10, GraphicsUnit.Pixel);
            //Arial 9
            Font messageFontArial_9 = new Font("Arial", 9, GraphicsUnit.Pixel);
            //Arial 7
            Font messageFontArial_7 = new Font("Arial", 7, GraphicsUnit.Pixel);

            

            //jednotlivé řádky průvodky
            //g.DrawString SYNTAXE (název_proměnné, FONT: messageFontArial_vetsi, ŠTĚTEC: Brushes.Black,X: 10,Y: 0);
            //první řádek
            string message0 = "Průvodka polotovaru";
            g.DrawString(message0, messageFontArial_20, Brushes.Black, 5, 5);
            string message01 = "Recept: ";
            g.DrawString(message01, messageFontArial_12, Brushes.Black, 5, 53);
            string message02 = cisloReceptuLabel.Text.ToString();
            g.DrawString(message02, messageFontArial_20, Brushes.Black, 130, 45);
            string message03 = "Barevné značení: ";
            g.DrawString(message03, messageFontArial_12, Brushes.Black, 180, 53);
            string message04 = barZnacTiskLabel.Text.ToString();
            g.DrawString(message04, messageFontArial_20, Brushes.Black, 320, 45);
            //druhý řádek
            string message1 = "Polotovar:";
            g.DrawString(message1, messageFontArial_12, Brushes.Black, 5, 88);
            string message11 = polotovarLabel.Text.ToString();
            g.DrawString(message11, messageFontArial_20, Brushes.Black, 130, 80);

            //třetí řádek
            string message2 = "Pro rozměr:";
            g.DrawString(message2, messageFontArial_12, Brushes.Black, 5, 123);
            string message21 = proRozmeryLabel2.Text.ToString();
            g.DrawString(message21, messageFontArial_20, Brushes.Black, 130, 115);

            //čtvrtý řádek
            string message3 = "Číslo směsi:";
            g.DrawString(message3, messageFontArial_12, Brushes.Black, 5, 158);
            string message31 = smesLabel.Text.ToString();
            g.DrawString(message31, messageFontArial_20, Brushes.Black, 130, 150);

            //pátý řádek
            string message4 = "Číslo dávky:";
            g.DrawString(message4, messageFontArial_12, Brushes.Black, 5, 193);
            string message41 = smesNumericUpDown1.Text.ToString() + "-" + smesNumericUpDown2.Text.ToString() + " / " + smesNumericUpDown3.Text.ToString() + "-" + smesNumericUpDown4.Text.ToString() + " / " + smesNumericUpDown5.Text.ToString() + "-" + smesNumericUpDown6.Text.ToString();
            g.DrawString(message41, messageFontArial_20, Brushes.Black, 130, 185);

            //šestý řádek 
            string message5 = "Zpracuj od:";
            g.DrawString(message5, messageFontArial_12, Brushes.Black, 5, 228);
            string message51 = "Zpracuj do:";
            g.DrawString(message51, messageFontArial_12, Brushes.Black, 5, 263);


            //šestý řádek - datum s časem
            string message6 = zpracOdLabel.Text.ToString();
            g.DrawString(message6, messageFontArial_20, Brushes.Black, 130, 220);
            string message61 = zpracDoLabel.Text.ToString();
            g.DrawString(message61, messageFontArial_20, Brushes.Black, 130, 255);

            //sedmý řádek 
            string message7 = "Vyrobil:";
            g.DrawString(message7, messageFontArial_12, Brushes.Black, 5, 298);
            string message71 = obsluhaComboBox.SelectedItem.ToString() + ", " + smenyComboBox.SelectedItem.ToString() + " směna";
            g.DrawString(message71, messageFontArial_20, Brushes.Black, 130, 290);

            //tabulka pro konfekci
            string message91 = "Materiál nahodil:";
            g.DrawString(message91, messageFontArial_7, Brushes.Black, 5, 415);
            string message92 = "Zkontroloval podle konf. předpisu:";
            g.DrawString(message92, messageFontArial_7, Brushes.Black, 5, 452);
            string message93 = "Jméno:";
            g.DrawString(message93, messageFontArial_7, Brushes.Black, 5, 489);
            string message94 = "Podpis:";
            g.DrawString(message94, messageFontArial_7, Brushes.Black, 5, 526);
            string message95 = "Zpracoval:";
            g.DrawString(message95, messageFontArial_7, Brushes.Black, 205, 415);
            string message96 = "Konfekční stroj:";
            g.DrawString(message96, messageFontArial_7, Brushes.Black, 205, 452);
            string message97 = "Podpis:";
            g.DrawString(message97, messageFontArial_7, Brushes.Black, 205, 489);
            string message98 = "Podpis:";
            g.DrawString(message98, messageFontArial_7, Brushes.Black, 205, 526);

            //jedenáctý řádek
            string message10 = "Mitas Zlín. Vyrobeno na lince Triplex, kód linky: 001, č. pr. ...............";//+ cisloPruvodkyLabel1.Text.ToString();
            g.DrawString(message10, messageFontArial_10, Brushes.Black, 5, 570); 
            #endregion
            #region matice čísel - pozice na průvodce
            //matice pozic
            /*
            for (int i = 0; i < 450; i = i + 50)
            {
                for (int j = 0; j < 600; j = j + 50)
                {
                    string message = i + ", " + j;
                    g.DrawString(message, messageFontArial_9, Brushes.Black, i, j);
                }
            }*/
            #endregion

        }
        //spuštění okna s tiskem
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                int j;
                int.TryParse(cisloPruvodkyLabel.Text, out j);
                int poslPruv = j + 1;
                string posledniPruvodka = poslPruv.ToString();
                databazePruvodek.PridejRadek(posledniPruvodka, cisloReceptuLabel.Text, smesLabel.Text, smes1NumericUpDown1.Text.ToString() + "-" + smes1NumericUpDown2.Text.ToString(), smes2NumericUpDown1.Text.ToString() + "-" + smes2NumericUpDown2.Text.ToString(), smes4NumericUpDown1.Text.ToString() + "-" + smes4NumericUpDown2.Text.ToString(), DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), obsluhaComboBox.SelectedItem.ToString());
                databazePruvodek.UlozRadek(posledniPruvodka, cisloReceptuLabel.Text, smesLabel.Text, smes1NumericUpDown1.Text.ToString() + "-" + smes1NumericUpDown2.Text.ToString(), smes2NumericUpDown1.Text.ToString() + "-" + smes2NumericUpDown2.Text.ToString(), smes4NumericUpDown1.Text.ToString() + "-" + smes4NumericUpDown2.Text.ToString(), DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), obsluhaComboBox.SelectedItem.ToString());
                cisloPruvodkyLabel.Text = posledniPruvodka;
                printPreviewDialog1.ShowDialog();
               
            }
            catch (Exception)
            {
                Console.WriteLine("Chyba tisku. Vyplnil jsi všechna důležitá pole?");
            }
           
        }

        #region označení textu pro zadávání čísel směsi - vybere text v kontrolce po focusu tabulátorem
        private void smes1NumericUpDown1_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown1.Select(0, smes1NumericUpDown1.Text.Length);
        }
        private void smes1NumericUpDown2_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown2.Select(0, smes1NumericUpDown2.Text.Length);
        }
        private void smes1NumericUpDown3_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown3.Select(0, smes1NumericUpDown3.Text.Length);
        }
        private void smes1NumericUpDown4_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown4.Select(0, smes1NumericUpDown4.Text.Length);
        }
        private void smes1NumericUpDown5_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown5.Select(0, smes1NumericUpDown5.Text.Length);
        }
        private void smes1NumericUpDown6_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown6.Select(0, smes1NumericUpDown6.Text.Length);
        }
        private void smes1NumericUpDown7_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown7.Select(0, smes1NumericUpDown7.Text.Length);
        }
        private void smes1NumericUpDown8_Enter(object sender, EventArgs e)
        {
            smes1NumericUpDown8.Select(0, smes1NumericUpDown8.Text.Length);
        }
        private void smes2NumericUpDown1_Enter(object sender, EventArgs e)
        {
            smes2NumericUpDown1.Select(0, smes2NumericUpDown1.Text.Length);
        }
        private void smes2NumericUpDown2_Enter(object sender, EventArgs e)
        {
            smes2NumericUpDown2.Select(0, smes2NumericUpDown2.Text.Length);
        }
        private void smes2NumericUpDown3_Enter(object sender, EventArgs e)
        {
            smes2NumericUpDown3.Select(0, smes2NumericUpDown3.Text.Length);
        }
        private void smes2NumericUpDown4_Enter(object sender, EventArgs e)
        {
            smes2NumericUpDown4.Select(0, smes2NumericUpDown4.Text.Length);
        }
        private void smes3NumericUpDown1_Enter(object sender, EventArgs e)
        {
            smes3NumericUpDown1.Select(0, smes3NumericUpDown1.Text.Length);
        }
        private void smes3NumericUpDown2_Enter(object sender, EventArgs e)
        {
            smes3NumericUpDown2.Select(0, smes3NumericUpDown2.Text.Length);
        }
        private void smes3NumericUpDown3_Enter(object sender, EventArgs e)
        {
            smes3NumericUpDown3.Select(0, smes3NumericUpDown3.Text.Length);
        }
        private void smes3NumericUpDown4_Enter(object sender, EventArgs e)
        {
            smes3NumericUpDown4.Select(0, smes3NumericUpDown4.Text.Length);
        }
        private void smes4NumericUpDown1_Enter(object sender, EventArgs e)
        {
            smes4NumericUpDown1.Select(0, smes4NumericUpDown1.Text.Length);
        }
        private void smes4NumericUpDown2_Enter(object sender, EventArgs e)
        {
            smes4NumericUpDown2.Select(0, smes4NumericUpDown2.Text.Length);
        }
        #endregion

        private void seznamPrůvodekToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PruvodkyForm pruvodky = new PruvodkyForm();//otevře novou instanci formuláře Pruvodky
            pruvodky.Show();
        }

        private void konecToolStripMenuItem_Click(object sender, EventArgs e) //konec aplikace
        {
            //databazePruvodek.Uloz();
            this.Close();
        }

        private void timer2_Tick(object sender, EventArgs e) //auto save na časovač
        {
            //databazePruvodek.Uloz();
        }

        private void nastaveníToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NastaveniForm nastaveni = new NastaveniForm();//otevře novou instanci formuláře Pruvodky
            nastaveni.Show();
        }

       
    }//tady končí třída "Form"
}//EOF
