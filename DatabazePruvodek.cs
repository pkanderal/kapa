﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Kapa_pro_triplex
{
    class DatabazePruvodek
    {
        
            private List<Pruvodka> pruvodka;
            private string souborP;

            public DatabazePruvodek(string souborP)
            {
            pruvodka = new List<Pruvodka>();
            this.souborP = souborP;
        }

            public void PridejRadek(string cisloPruvodky, string cisloReceptuPruvodky, string cisloSmesi, string cisloDavky1, string cisloDavky2, string cisloDavky3, string vyrobil, string datumTisku, string casTisku)
            {
            Pruvodka p = new Pruvodka(cisloPruvodky, cisloReceptuPruvodky, cisloSmesi, cisloDavky1, cisloDavky2, cisloDavky3, vyrobil, datumTisku, casTisku);
            pruvodka.Add(p);
        }
       
            public Pruvodka[] VratVsechny()
            {
            return pruvodka.ToArray();

            }

            public void Uloz()
            {
            string cesta = "";
            try
            {
                cesta = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Kapa_tri");
                if (!Directory.Exists(cesta))
                    Directory.CreateDirectory(cesta);
            }
            catch
            {
                Console.WriteLine("Nepodařilo se vytvořit složku {0}, zkontrolujte prosím svá oprávnění.", cesta);
            }
            // otevření souboru pro zápis
            using (var sw = new StreamWriter(
                new FileStream(souborP,
                FileMode.Open,
                FileAccess.Write),
                Encoding.Default))
                
            {
                sw.WriteLine("cisloPruvodky;cisloReceptu;cisloSmesi;cisloDavek;cisloDavek;cisloDavek;datumTisku;casTisku;vyrobil");
                foreach (Pruvodka p in pruvodka)
                {
                    // vytvoření pole hodnot
                    string[] hodnoty = { p.CisloPruvodky, p.CisloReceptuPruvodky, p.CisloSmesi, p.CisloDavky1, p.CisloDavky2, p.CisloDavky3, p.Vyrobil, p.DatumTisku, p.CasTisku };
                    // vytvoření řádku
                    string radek = String.Join(";", hodnoty);
                    // zápis řádku
                    sw.WriteLine(radek);
                }
                sw.Flush();
            }
            
        }
        public void UlozRadek(string cisloPruvodky, string cisloReceptuPruvodky, string cisloSmesi, string cisloDavky1, string cisloDavky2, string cisloDavky3, string vyrobil, string datumTisku, string casTisku)
        {
            string cesta = "";
            try
            {
                cesta = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Kapa_tri");
                if (!Directory.Exists(cesta))
                    Directory.CreateDirectory(cesta);
            }
            catch
            {
                Console.WriteLine("Nepodařilo se vytvořit složku {0}, zkontrolujte prosím svá oprávnění.", cesta);
            }
            // otevření souboru pro zápis
            using (var sw = new StreamWriter(souborP, true, Encoding.Default))

            {

                // vytvoření pole hodnot
                string[] hodnoty = { cisloPruvodky, cisloReceptuPruvodky, cisloSmesi, cisloDavky1, cisloDavky2, cisloDavky3, vyrobil, datumTisku, casTisku };
                // vytvoření řádku
                string radek = String.Join(";", hodnoty);
                // zápis řádku
                sw.WriteLine(radek);
                sw.Flush();
            }
                
            }
        public string cisloPosledniPruvodky { get; private set; }
        public void NactiCisloPosledniPruvodky()
        {
            string m = "";
            StreamReader r = new StreamReader(souborP, Encoding.Default);
            while (r.EndOfStream == false)
            {
                m = r.ReadLine();
            }
            string[] rozdeleno = m.Split(';');
            cisloPosledniPruvodky = rozdeleno[0];
            //Console.WriteLine("{0}", m);
            r.Close();
            
        }
        
        /// <summary>
        /// Načítá data ze souboru
        /// </summary>
        public void Nacti()
        {
            pruvodka.Clear();
            //Otevře soubor pro čtení
            //Encoding.Default potřebuje using System.Text;
            //opravuje načítání diakritiky ze souboru                                  
            using (StreamReader sr = new StreamReader(souborP, Encoding.Default))
            {
                string radekNadpisu = sr.ReadLine(); //načtení prvního řádku s popisy sloupců
                // = přeskočení prvního řádku uložením do proměnné s jiným názvem)
                string p;
                // čte řádek po řádku
                while ((p = sr.ReadLine()) != null)
                {
                    // rozdělí string řádku podle středníků
                    string[] rozdeleno = p.Split(';');
                    string cisloPruvodky = rozdeleno[0];
                    string cisloReceptuPruvodky = rozdeleno[1];
                    string cisloSmesi = rozdeleno[2];
                    string cisloDavky1 = rozdeleno[3];
                    string cisloDavky2 = rozdeleno[4];
                    string cisloDavky3 = rozdeleno[5];
                    string datumTisku = rozdeleno[6];
                    string casTisku = rozdeleno[7];
                    string vyrobil = rozdeleno[8];


                    PridejRadek(cisloPruvodky, cisloReceptuPruvodky, cisloSmesi, cisloDavky1, cisloDavky2, cisloDavky3, vyrobil, datumTisku, casTisku);
                }
            }
        }
        public override string ToString()
        {
            return cisloPosledniPruvodky;
        }


    }
}
