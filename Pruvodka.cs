﻿namespace Kapa_pro_triplex
{
    class Pruvodka
    {
        public string CisloPruvodky { get; private set; }
        public string CisloReceptuPruvodky { get; private set; }
        public string CisloSmesi { get; private set; }
        public string CisloDavky1 { get; private set; }
        public string CisloDavky2 { get; private set; }
        public string CisloDavky3 { get; private set; }
        public string Vyrobil { get; private set; }
        public string DatumTisku { get; private set; }
        public string CasTisku { get; private set; }



        public Pruvodka(string cisloPruvodky, string cisloReceptuPruvodky,string cisloSmesi, string cisloDavky1, string cisloDavky2, string cisloDavky3, string vyrobil, string datumTisku, string casTisku)
        {
            CisloPruvodky = cisloPruvodky;
            CisloReceptuPruvodky = cisloReceptuPruvodky;
            CisloSmesi = cisloSmesi;
            CisloDavky1 = cisloDavky1;
            CisloDavky2 = cisloDavky2;
            CisloDavky3 = cisloDavky3;
            Vyrobil = vyrobil;
            DatumTisku = datumTisku;
            CasTisku = casTisku;
        }

        public override string ToString()
        {
            return CisloPruvodky;
        }

    }
}
