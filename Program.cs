﻿using System;
using System.Windows.Forms;

namespace Kapa_pro_triplex
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new hlavniOknoTriplexForm());
        }
    }
}
