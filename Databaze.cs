﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Kapa_pro_triplex
{
    /// <summary>
    /// Třída Databáze
    /// </summary>
    class Databaze //vytvářím třídu Databaze
    {

        private List<Recept> recepty;
        private string soubor;

        public Databaze(string soubor) //soubor s recepty
        {
            recepty = new List<Recept>();
            this.soubor = soubor;
        }

        public void PridejRecept(string cisloReceptu, string cisloReceptuTroester, string polotovar, string rozmer, string smes220, string smes150, string smes120, string smes70, string barZnac1, string barZnac2, string barZnac3, string barZnac4, string datumAkt)
        {
            Recept r = new Recept(cisloReceptu, cisloReceptuTroester, polotovar, rozmer, smes220, smes150, smes120, smes70, barZnac1, barZnac2, barZnac3, barZnac4, datumAkt);
            recepty.Add(r);
        }


        public Recept[] VratVsechny() //pole Recept
        {
            return recepty.ToArray();
        }

        /// <summary>
        /// Ukládá recept do souboru
        /// </summary>
        
        public void Uloz()
        {
            string cesta = "";
            try
            {
                cesta = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Kapa_tri");
                if (!Directory.Exists(cesta))
                    Directory.CreateDirectory(cesta);
            }
            catch
            {
                Console.WriteLine("Nepodařilo se vytvořit složku {0}, zkontrolujte prosím svá oprávnění.", cesta);
            }
           
            // otevření souboru pro zápis
            using (StreamWriter sw = new StreamWriter(soubor))
            {
                // projetí uživatelů
                foreach (Recept r in recepty)
                {
                    // vytvoření pole hodnot
                    string[] hodnoty = { r.CisloReceptu, r.CisloReceptuTroester, r.Polotovar, r.Rozmer, r.Smes220, r.Smes150, r.Smes120, r.Smes70, r.BarZnac1, r.BarZnac2, r.BarZnac3, r.BarZnac4, r.DatumAkt };
                    // vytvoření řádku
                    string radek = String.Join(";", hodnoty);
                    // zápis řádku
                    sw.WriteLine(radek);
                }
                // vyprázdnění bufferu
                sw.Flush();
            }
            
        }
        
        /// <summary>
        /// Načítá data ze souboru
        /// </summary>
        public void Nacti()
        {
            recepty.Clear();
            //Otevře soubor pro čtení
            //Encoding.Default potřebuje using System.Text;
            //opravuje načítání diakritiky ze souboru                                  
            using (StreamReader sr = new StreamReader(soubor, Encoding.Default))
            {
                string radekNadpisu = sr.ReadLine(); //načtení prvního řádku s popisy sloupců
                // = přeskočení prvního řádku uložením do proměnné s jiným názvem)
                string s;
                // čte řádek po řádku
                while ((s = sr.ReadLine()) != null)
                {
                    // rozdělí string řádku podle středníků
                    string[] rozdeleno = s.Split(';');
                    string cisloReceptu = rozdeleno[0];
                    string cisloReceptuTroester = rozdeleno[1];
                    string polotovar = rozdeleno[2];
                    string rozmer = rozdeleno[3];
                    string smes220 = rozdeleno[4];
                    string smes150 = rozdeleno[5];
                    string smes120 = rozdeleno[6];
                    string smes70 = rozdeleno[7];
                    string barZnac1 = rozdeleno[8];
                    string barZnac2 = rozdeleno[9];
                    string barZnac3 = rozdeleno[10];
                    string barZnac4 = rozdeleno[11];
                    string datumAkt = rozdeleno[12];

                    PridejRecept(cisloReceptu, cisloReceptuTroester, polotovar, rozmer, smes220, smes150, smes120, smes70, barZnac1, barZnac2, barZnac3, barZnac4, datumAkt);
                }
            }
        }
    }

}
