﻿using System;

namespace Kapa_pro_triplex
{
    public class Recept
    {
        public string CisloReceptu { get; private set; } 

        public string CisloReceptuTroester { get; private set; }

        public string Polotovar { get; private set; }

        public string Rozmer { get; private set; } 

        public string Smes220 { get; private set; }
        public string Smes150 { get; private set; }
        public string Smes120 { get; private set; }
        public string Smes70 { get; private set; }

        public string BarZnac1 { get; private set; } 

        public string BarZnac2 { get; private set; } 

        public string BarZnac3 { get; private set; } 

        public string BarZnac4 { get; private set; } 
               
        public string DatumAkt { get; private set; }

       
        public Recept(string cisloReceptu, string cisloReceptuTroester, string polotovar, string rozmer, string smes220, string smes150, string smes120, string smes70, string barZnac1, string barZnac2, string barZnac3, string barZnac4, string datumAkt)
        {
            CisloReceptu = cisloReceptu;
            CisloReceptuTroester = cisloReceptuTroester;
            Polotovar = polotovar;
            Rozmer = rozmer;
            Smes220 = smes220;
            Smes150 = smes150;
            Smes120 = smes120;
            Smes70 = smes70;
            BarZnac1 = barZnac1;
            BarZnac2 = barZnac2;
            BarZnac3 = barZnac3;
            BarZnac4 = barZnac4;
            DatumAkt = datumAkt;
        }

        /// <summary>
        /// Vrací název řádku receptu
        /// </summary>
        /// <returns>Vrací název řádku receptu</returns>
        public override string ToString()
        {
            return CisloReceptu + " (" + CisloReceptuTroester+")";
        }

    }

}

