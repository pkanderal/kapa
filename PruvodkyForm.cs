﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Kapa_pro_triplex
{
    public partial class PruvodkyForm : Form
    {
        private DatabazePruvodek databazePruvodek;
        public PruvodkyForm()
        {
            //vymazání labelů při počátečním načtení formuláře
            #region inicializace okna a vymazání labelů
            InitializeComponent();
            cisloPruvodkyLabel.Text = "";
            cisloReceptuLabel.Text = "";
            cisloSmesiLabel.Text = "";
            cisloDavekLabel1.Text = "";
            cisloDavekLabel2.Text = "";
            cisloDavekLabel3.Text = "";
            datumTiskuLabel.Text = "";
            casTiskuLabel.Text = "";
            vyrobilLabel.Text = "";
            #endregion
            #region cesta do AppData
            string cesta = "";
            try
            {
                cesta = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "kapa_tri");
                if (!Directory.Exists(cesta))
                    Directory.CreateDirectory(cesta);

            }
            catch
            {
                Console.WriteLine("Nepodařilo se vytvořit složku {0}, zkontrolujte prosím svá oprávnění.", cesta);
            }
            #endregion
            #region načtení databaze (seznamu průvodek)
            databazePruvodek = new DatabazePruvodek(cesta + "\\" + "pruvodky.csv");
            try
            {
                databazePruvodek.Nacti();
                pruvodkyListBox1.Items.Clear();
                pruvodkyListBox1.Items.AddRange(databazePruvodek.VratVsechny());
            }
            catch
            {
                MessageBox.Show("Databázi průvodek se nepodařilo načíst, soubor zřejmě neexistuje nebo je otevřen jinou aplikací.",
                        "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
             
        }

        private void pruvodkyListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pruvodkyListBox1.SelectedItem != null)
            {
                Pruvodka vybrany = (Pruvodka)pruvodkyListBox1.SelectedItem;
                cisloPruvodkyLabel.Text = vybrany.CisloPruvodky;
                cisloReceptuLabel.Text = vybrany.CisloReceptuPruvodky; 
                cisloSmesiLabel.Text = vybrany.CisloSmesi;
                cisloDavekLabel1.Text = vybrany.CisloDavky1;
                cisloDavekLabel2.Text = vybrany.CisloDavky2;
                cisloDavekLabel3.Text = vybrany.CisloDavky3;
                datumTiskuLabel.Text = vybrany.DatumTisku;
                casTiskuLabel.Text = vybrany.CasTisku;
                vyrobilLabel.Text = vybrany.Vyrobil;
            }
    }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pageSetupDialog1.ShowDialog();
            printPreviewDialog1.ShowDialog();
        }

        private void printDocument2_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {

        }
    }
}
