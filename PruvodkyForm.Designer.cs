﻿namespace Kapa_pro_triplex
{
    partial class PruvodkyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PruvodkyForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pruvodkyListBox1 = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.casTiskuLabel = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cisloDavekLabel2 = new System.Windows.Forms.Label();
            this.vyrobilLabel = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cisloDavekLabel3 = new System.Windows.Forms.Label();
            this.datumTiskuLabel = new System.Windows.Forms.Label();
            this.cisloDavekLabel1 = new System.Windows.Forms.Label();
            this.cisloSmesiLabel = new System.Windows.Forms.Label();
            this.cisloReceptuLabel = new System.Windows.Forms.Label();
            this.cisloPruvodkyLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.printDocument2 = new System.Drawing.Printing.PrintDocument();
            this.printPreviewDialog1 = new System.Windows.Forms.PrintPreviewDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pruvodkyListBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(5, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 276);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seznam průvodek";
            // 
            // pruvodkyListBox1
            // 
            this.pruvodkyListBox1.FormattingEnabled = true;
            this.pruvodkyListBox1.ItemHeight = 20;
            this.pruvodkyListBox1.Location = new System.Drawing.Point(3, 25);
            this.pruvodkyListBox1.Name = "pruvodkyListBox1";
            this.pruvodkyListBox1.Size = new System.Drawing.Size(166, 244);
            this.pruvodkyListBox1.TabIndex = 0;
            this.pruvodkyListBox1.SelectedIndexChanged += new System.EventHandler(this.pruvodkyListBox1_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.casTiskuLabel);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cisloDavekLabel2);
            this.groupBox2.Controls.Add(this.vyrobilLabel);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cisloDavekLabel3);
            this.groupBox2.Controls.Add(this.datumTiskuLabel);
            this.groupBox2.Controls.Add(this.cisloDavekLabel1);
            this.groupBox2.Controls.Add(this.cisloSmesiLabel);
            this.groupBox2.Controls.Add(this.cisloReceptuLabel);
            this.groupBox2.Controls.Add(this.cisloPruvodkyLabel);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox2.Location = new System.Drawing.Point(186, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(323, 243);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Identifikační údaje průvodky";
            // 
            // casTiskuLabel
            // 
            this.casTiskuLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.casTiskuLabel.Location = new System.Drawing.Point(200, 182);
            this.casTiskuLabel.Name = "casTiskuLabel";
            this.casTiskuLabel.Size = new System.Drawing.Size(100, 20);
            this.casTiskuLabel.TabIndex = 17;
            this.casTiskuLabel.Text = "casTisku";
            this.casTiskuLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(6, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 16;
            this.label6.Text = "Čas tisku:";
            // 
            // cisloDavekLabel2
            // 
            this.cisloDavekLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloDavekLabel2.Location = new System.Drawing.Point(138, 122);
            this.cisloDavekLabel2.Name = "cisloDavekLabel2";
            this.cisloDavekLabel2.Size = new System.Drawing.Size(162, 20);
            this.cisloDavekLabel2.TabIndex = 15;
            this.cisloDavekLabel2.Text = "davky2";
            this.cisloDavekLabel2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // vyrobilLabel
            // 
            this.vyrobilLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.vyrobilLabel.Location = new System.Drawing.Point(200, 202);
            this.vyrobilLabel.Name = "vyrobilLabel";
            this.vyrobilLabel.Size = new System.Drawing.Size(100, 20);
            this.vyrobilLabel.TabIndex = 14;
            this.vyrobilLabel.Text = "vyrobilLabel";
            this.vyrobilLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(6, 202);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 16);
            this.label12.TabIndex = 13;
            this.label12.Text = "Vyrobil:";
            // 
            // cisloDavekLabel3
            // 
            this.cisloDavekLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloDavekLabel3.Location = new System.Drawing.Point(110, 142);
            this.cisloDavekLabel3.Name = "cisloDavekLabel3";
            this.cisloDavekLabel3.Size = new System.Drawing.Size(190, 20);
            this.cisloDavekLabel3.TabIndex = 12;
            this.cisloDavekLabel3.Text = "davky3";
            this.cisloDavekLabel3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // datumTiskuLabel
            // 
            this.datumTiskuLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.datumTiskuLabel.Location = new System.Drawing.Point(200, 162);
            this.datumTiskuLabel.Name = "datumTiskuLabel";
            this.datumTiskuLabel.Size = new System.Drawing.Size(100, 20);
            this.datumTiskuLabel.TabIndex = 11;
            this.datumTiskuLabel.Text = "datumTisku";
            this.datumTiskuLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cisloDavekLabel1
            // 
            this.cisloDavekLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloDavekLabel1.Location = new System.Drawing.Point(138, 102);
            this.cisloDavekLabel1.Name = "cisloDavekLabel1";
            this.cisloDavekLabel1.Size = new System.Drawing.Size(162, 20);
            this.cisloDavekLabel1.TabIndex = 10;
            this.cisloDavekLabel1.Text = "davky1";
            this.cisloDavekLabel1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cisloSmesiLabel
            // 
            this.cisloSmesiLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloSmesiLabel.Location = new System.Drawing.Point(200, 82);
            this.cisloSmesiLabel.Name = "cisloSmesiLabel";
            this.cisloSmesiLabel.Size = new System.Drawing.Size(100, 20);
            this.cisloSmesiLabel.TabIndex = 9;
            this.cisloSmesiLabel.Text = "smesi";
            this.cisloSmesiLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cisloReceptuLabel
            // 
            this.cisloReceptuLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloReceptuLabel.Location = new System.Drawing.Point(200, 42);
            this.cisloReceptuLabel.Name = "cisloReceptuLabel";
            this.cisloReceptuLabel.Size = new System.Drawing.Size(100, 20);
            this.cisloReceptuLabel.TabIndex = 8;
            this.cisloReceptuLabel.Text = "cisloReceptu";
            this.cisloReceptuLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cisloPruvodkyLabel
            // 
            this.cisloPruvodkyLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cisloPruvodkyLabel.Location = new System.Drawing.Point(200, 22);
            this.cisloPruvodkyLabel.Name = "cisloPruvodkyLabel";
            this.cisloPruvodkyLabel.Size = new System.Drawing.Size(100, 20);
            this.cisloPruvodkyLabel.TabIndex = 7;
            this.cisloPruvodkyLabel.Text = "cisloPruvodkyLabel";
            this.cisloPruvodkyLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(6, 162);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "Datum tisku:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(6, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Číslo dávek:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Číslo směsi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Číslo receptu:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(6, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 16);
            this.label1.TabIndex = 2;
            this.label1.Text = "Číslo průvodky:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(427, 261);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Zavřít";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(324, 261);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Tisk průvodky";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // printDocument2
            // 
            this.printDocument2.DocumentName = "document1";
            this.printDocument2.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument2_PrintPage);
            // 
            // printPreviewDialog1
            // 
            this.printPreviewDialog1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printPreviewDialog1.ClientSize = new System.Drawing.Size(400, 300);
            this.printPreviewDialog1.Document = this.printDocument2;
            this.printPreviewDialog1.Enabled = true;
            this.printPreviewDialog1.Icon = ((System.Drawing.Icon)(resources.GetObject("printPreviewDialog1.Icon")));
            this.printPreviewDialog1.Name = "printPreviewDialog1";
            this.printPreviewDialog1.Visible = false;
            // 
            // pageSetupDialog1
            // 
            this.pageSetupDialog1.Document = this.printDocument2;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(6, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 16);
            this.label8.TabIndex = 19;
            this.label8.Text = "Barevné značení:";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(200, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 20);
            this.label7.TabIndex = 20;
            this.label7.Text = "cisloReceptu";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // PruvodkyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 296);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(530, 335);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(530, 335);
            this.Name = "PruvodkyForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Průvodky";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox pruvodkyListBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label cisloDavekLabel3;
        private System.Windows.Forms.Label datumTiskuLabel;
        private System.Windows.Forms.Label cisloDavekLabel1;
        private System.Windows.Forms.Label cisloSmesiLabel;
        private System.Windows.Forms.Label cisloReceptuLabel;
        private System.Windows.Forms.Label cisloPruvodkyLabel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label vyrobilLabel;
        private System.Windows.Forms.Label cisloDavekLabel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label casTiskuLabel;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button2;
        private System.Drawing.Printing.PrintDocument printDocument2;
        private System.Windows.Forms.PrintPreviewDialog printPreviewDialog1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}